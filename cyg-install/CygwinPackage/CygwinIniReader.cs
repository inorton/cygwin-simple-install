﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace CygwinPackage
{
    /// <summary>
    /// A parser for setup.ini files from cygwin mirrors
    /// </summary>
    public class CygwinIniReader : HintReader<CygwinPackageRecord>
    {
        const String PackageStart = "@ ";

        /// <summary>
        /// Read an ini file from disk
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public IDictionary<String, CygwinPackageRecord> Read(string filename)
        {
            using (var file = File.Open(filename, FileMode.Open))
            {
                var reader = new StreamReader(file);
                return this.Read(reader);
            }
        }

        /// <summary>
        /// Read an ini file from a url
        /// </summary>
        /// <param name="webaddress"></param>
        /// <returns></returns>
        public IDictionary<String, CygwinPackageRecord> Read(Uri webaddress)
        {
            using (var web = new WebClient())
            {
                var stream = web.OpenRead(webaddress);
                var reader = new StreamReader(stream);
                return this.Read(reader);
            }
        }

        public IDictionary<String, CygwinPackageRecord> Read(StreamReader reader)
        {
            var packages = new Dictionary<String, CygwinPackageRecord> ();

            // read whole packages at a time
            var currentPackage = new List<String>();
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (line.StartsWith(PackageStart))
                {
                    // the lines we have should be a package block
                    if (currentPackage.Count > 0)
                    {
                        if (currentPackage[0].StartsWith(PackageStart))
                        {
                            // we've just found a new package, process the previous one
                            var package = this.Parse(currentPackage);
                            packages[package.Name] = package;
                        }
                        currentPackage.Clear();
                    }
                }
                currentPackage.Add(line);
            }

            return packages;
        }

        /// <summary>
        /// Handle lines of the package sections
        /// </summary>
        /// <param name="hint"></param>
        /// <param name="line"></param>
        public override void UnknownLine(CygwinPackageRecord hint, string line)
        {
            if (line.StartsWith(PackageStart)) {
                // new package
                var split = line.Trim().Split(new char[] { ' ' }, 2);
                hint.Name = split[1];
            }

            if (line.StartsWith("[prev]") && hint.LatestVersion == null)
                throw new CygwinPackageInitParseError("missing expected version before [prev]");
            
            if (line.Contains(":"))
            {
                var trimmed = line.Trim();
                var split = trimmed.Split(new char[] { ':' }, 2);

                switch (split[0])
                {
                    case "version":
                        if (hint.LatestVersion == null)
                        {
                            hint.LatestVersion = split[1].Trim();
                        }                        
                        hint.InstallVersions.Add(split[1].Trim(), null);

                        break;
                    case "install":
                        // fill in the location of the first null version
                        foreach (var pair in hint.InstallVersions.ToArray())
                        {
                            if (pair.Value == null)
                            {
                                hint.InstallVersions[pair.Key] = split[1].Trim();
                                break;
                            }
                        }
                        break;
                }
            }
        }
    }
}
