﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CygwinPackage
{
    /// <summary>
    /// Some error generated by CygwinPackage
    /// </summary>
    public abstract class CygwinPackageError : Exception
    {
        public CygwinPackageError() : base () { }
        public CygwinPackageError(String message) : base(message) { }
    }

    public class CygwinPackageInitParseError : CygwinPackageError
    {
        public  CygwinPackageInitParseError(String message) : base(message) { }
    }

    public class CygwinPackageNotFound : CygwinPackageError
    {
        public CygwinPackageNotFound(): base() { }
        public CygwinPackageNotFound(String message) : base(message) { }
    }

    public class CygwinPackageNameNotFound : CygwinPackageNotFound
    {
        public String Name { get; private set; }
        public CygwinPackageNameNotFound(String name)
        {
            this.Name = name;
        }
    }

    public class CygwinPackageVersionNotFound : CygwinPackageNotFound
    {
    }

    public class CygwinPackageInstallFailed : CygwinPackageError
    {
    }
}
