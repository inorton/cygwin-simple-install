﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CygwinPackage
{
    /// <summary>
    /// Represent a .hint file
    /// </summary>
    public class CygwinPackageHint
    {   
        public CygwinPackageHint()
        {
            this.Requires = new String[0];
        }

        /// <summary>
        /// The category value
        /// </summary>
        public String Category { get; set; }

        /// <summary>
        /// The sdisc value
        /// </summary>
        public String ShortDescription { get; set; }

        /// <summary>
        /// The ldisc value
        /// </summary>
        public String LongDescription { get; set; }

        /// <summary>
        /// The requires value
        /// </summary>
        public String[] Requires { get; set; }
    }
}
