﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using SharpCompress;


namespace CygwinPackage
{
    /// <summary>
    /// Given a package mirror, name and optional version, install it
    /// </summary>
    public class CygwinPackageInstaller
    {
        const String InstalledDBFile = "installed.db";

        /// <summary>
        /// The cygwin root
        /// </summary>
        public String Root { get; private set; }

        /// <summary>
        /// The cygwin mirror
        /// </summary>
        public String Mirror { get; private set; }

        /// <summary>
        /// The index of availible packages
        /// </summary>
        public IDictionary<String, CygwinPackageRecord> Packages { get; private set; }

        /// <summary>
        /// Get an index of the packages already installed
        /// </summary>
        /// <returns></returns>
        public IDictionary<String, String> GetInstalledPackages()
        {
            var installed = new Dictionary<String, String>();
            
            var dbfile = Path.Combine(Root, "etc", InstalledDBFile);
            if (File.Exists(dbfile)) {
                var lines = File.ReadAllLines(dbfile);
                foreach (var line in lines)
                {
                    // we don't understand the full format, just
                    // "PKGNAME TARBALL_BASENAME DISCARD..."
                    var split = line.Split(' ');
                    if (split.Length > 2)
                    {
                        var name = split[0].Trim();
                        var tarball = split[1].Trim();
                        installed[name] = tarball;
                    }
                }
            }

            return installed;
        }

        /// <summary>
        /// For the given package name, if it is installed, return the files that are installed
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public IList<String> GetInstalledFiles(String package)
        {
            var files = new List<String>();
            var installed = GetInstalledPackages();
            if (!installed.ContainsKey(package))
            {
                throw new CygwinPackageNotFound(
                    String.Format(
                        "package {0} not installed in {1}", package, Root));
            }

            var listfile = Path.Combine(Root, "etc", String.Format("{0}.lst.gz", package));
            if (!File.Exists(listfile))
            {
                throw new CygwinPackageNotFound(
                    String.Format(
                        "missing expected {0}", listfile));
            }

            // decompress the file and read the content
            using (var filereader = File.OpenRead(listfile)) {
                var gzreader = SharpCompress.Readers.GZip.GZipReader.Open(filereader);
                var entry = gzreader.OpenEntryStream();
                var entry_stream = new StreamReader(entry);
                while (!entry_stream.EndOfStream)
                {
                    var line = entry_stream.ReadLine().Trim();
                    // change to windows file paths
                    var winfile = line.Replace("/", "\\");
                    files.Add(Path.Combine(Root, winfile));
                }
            }

            return files;
        }

        private CygwinIniReader inireader = new CygwinIniReader();

        /// <summary>
        /// Make a package installer for the given cygwin root
        /// </summary>
        /// <param name="rootdir"></param>
        public CygwinPackageInstaller(String rootdir)
        {
            Root = rootdir;
            
        }

        /// <summary>
        /// Get the package list from the mirror and remember the mirror
        /// </summary>
        /// <param name="mirror"></param>
        /// <returns></returns>
        public IDictionary<String, CygwinPackageRecord> GetPackageList(String mirror)
        {
            this.Packages = this.GetPackages(mirror);            
            return this.Packages;
        }

        /// <summary>
        /// Get the package list from the mirror and remember the mirror
        /// </summary>
        /// <param name="mirror"></param>
        /// <returns></returns>
        IDictionary<String, CygwinPackageRecord> GetPackages(String mirror)
        {
            this.Mirror = mirror;
            if (Uri.IsWellFormedUriString(mirror, UriKind.Absolute))
            {
                return inireader.Read(new Uri(mirror));
            } else
            {
                return inireader.Read(mirror);
            }
        }

        /// <summary>
        /// Install a package from the given mirror
        /// </summary>        
        /// <param name="package"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public bool Install(String package, String version)
        {
            if (!this.Packages.ContainsKey(package))
                throw new CygwinPackageNameNotFound(package);

            return false;
        }
    }
}
