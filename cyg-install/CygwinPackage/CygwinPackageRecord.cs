﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CygwinPackage
{
    public class CygwinPackageRecord : CygwinPackageHint
    {
        public CygwinPackageRecord() : base()
        {
            this.InstallVersions = new Dictionary<string, string>();
        }

        /// <summary>
        /// The latest version of this package
        /// </summary>
        public String LatestVersion { get; set; }

        /// <summary>
        /// The name of this package
        /// </summary>
        public String Name { get; set; }
        
        /// <summary>
        /// The location details for each availible version
        /// </summary>
        public Dictionary<String, String> InstallVersions { get; set; }
    }
}
