﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CygwinPackage
{
    public class HintReader : HintReader<CygwinPackageHint>
    {
        /// <summary>
        /// Load a .hint file from disk
        /// </summary>
        /// <param name="filepath"></param>
        public CygwinPackageHint Open(String filepath)
        {
            var lines = File.ReadAllLines(filepath);
            return Parse(lines);
        }
    }

    /// <summary>
    /// Understand cygwin .hint files
    /// </summary>
    public class HintReader<T> where T : CygwinPackageHint, new()
    {
        public virtual void UnknownLine(T hint, String line)
        {

        }

        /// <summary>
        /// Parse one block/entry
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        public T Parse(IEnumerable<String> lines)
        {
            var hint = new T();
            var requires = new List<string>();
            bool parsing_ldesc = false;            

            foreach (var line in lines)
            {
                var split = line.Split(new char[] { ':' }, 2);                
                var keyname = split[0].Trim(' ');
                var value = split.Last();
                var trimmed_line = line.TrimEnd(' ', '\r', '\n', '\f', '\t');
                if (split.Length == 2)
                {
                    value = split[1].Trim(' ', '\r', '\n', '\f', '\t');                    
                }
                
                switch (keyname)
                {
                    case "category":
                        hint.Category = value;
                        break;
                    case "sdesc":
                        hint.ShortDescription = value.Trim('"');
                        break;
                    case "requires":
                        var depends = value.Split(' ').ToArray();
                        hint.Requires = depends;
                        break;
                    case "ldesc":                        
                        hint.LongDescription = value.TrimStart('"');
                        parsing_ldesc = !value.EndsWith("\"");
                        hint.LongDescription = hint.LongDescription.TrimEnd('"');
                        break;
                    default:
                        // append more to ldisc
                        if (parsing_ldesc)
                        {
                            hint.LongDescription += "\n" + trimmed_line.TrimEnd('"');
                            parsing_ldesc = !trimmed_line.EndsWith("\"");
                        } else
                        {
                            this.UnknownLine(hint, trimmed_line);
                        }
                        break;
                }
            }

            return hint;
        }
    }
}
