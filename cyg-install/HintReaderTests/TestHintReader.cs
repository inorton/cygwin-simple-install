﻿using System;
using System.IO;
using NUnit.Framework;
using CygwinPackage;
using System.Reflection;

namespace HintReaderTests
{
    [TestFixture]
    public class TestHintReader
    {
        private static String GetTestDataPath(params String[] path)
        {
            var testlib = typeof(TestHintReader).Assembly.Location;
            var testdir = Path.GetDirectoryName(testlib);
            return Path.Combine(testdir, Path.Combine(path));
        }

        [Test]
        public void TestParseBasicFile()
        {
            var reader = new HintReader();
            var hint = reader.Open(GetTestDataPath("HintFiles", "bash-4.4.12-3.hint"));
            Assert.IsNotNull(hint);
            Assert.AreEqual("Base Shells", 
                hint.Category);
            Assert.AreEqual("The GNU Bourne Again SHell", 
                hint.ShortDescription);

            Assert.AreEqual("Bash is an sh-compatible shell that incorporates useful features\nfrom the Korn shell (ksh) and C shell (csh).  It is intended to conform to the\nIEEE POSIX P1003.2/ISO 9945.2 Shell and Tools standard.  It offers functional\nimprovements over sh for both programming and interactive use. In addition,\nmost sh scripts can be run by Bash without modification.",
                hint.LongDescription);            
        }

        [Test]
        public void TestParsePacakgeIniSection()
        {
            var block = @"@ abiword-debuginfo
sdesc: ""Debug info for abiword""
ldesc: ""This package contains files necessary for debugging the
abiword package with gdb.""
category: Debug
requires: cygwin-debuginfo
version: 3.0.2-2
install: x86_64/release/abiword/abiword-debuginfo/abiword-debuginfo-3.0.2-2.tar.xz 20361760 c6418d323115f5c58154e10620fe4731efed7faa807982aa4adf38e754ce6d55145965e138c42d35f440700adbc04d967b4cce798148f813648808fa6947b1fd
source: x86_64/release/abiword/abiword-3.0.2-2-src.tar.xz 11051028 d9a6b685587dd539080e739e200f55c54015b7c8c67997f940d7730199496257bf19fc55f69fda664d7eff6d85a01c04a74ff21bebaa42ddaa2c4b549fe95125
[prev]
version: 3.0.1-6
install: x86_64/release/abiword/abiword-debuginfo/abiword-debuginfo-3.0.1-6.tar.xz 19026648 47ad730c5af9b9eb5a488b32cfb120abe68aee88f59c90ad1e3c7e9b2345f0adb83c1906328c1176e7b5902493eef11856f56307db492d63458536bbc3ce763b
source: x86_64/release/abiword/abiword-3.0.1-6-src.tar.xz 11025944 2a62a0a7b8f100320f12d445e8c969323e1231fd17e8854e4335cc6f906bd46e6d1b339dbd4947663c701b291dbd43a8cce0cd4e5eeec5e96c5d72e31463028b
[prev]
version: 3.0.2-1
install: x86_64/release/abiword/abiword-debuginfo/abiword-debuginfo-3.0.2-1.tar.xz 20347860 17a5f6f0ad7b971d3ebbbfe1e095d7f898b08667073d69f9dc88f27439aaf83b4426e6dbc962244f6d3e503226b5908a9c0082ac1e6e9cb288aa4186f3cb51f0
source: x86_64/release/abiword/abiword-3.0.2-1-src.tar.xz 11050812 b36f1f0c186a333b8cab1e9a54f2eb7a654cec8df6403fa5bdece321e4e6f96182c4d24c75a1261868fce6768f45fc1e0eea92e2516b5a3058b55fec688f54ae
";

            var reader = new CygwinIniReader();
            var entry = reader.Parse(block.Split('\n'));

            Assert.IsNotNull(entry);
            Assert.AreEqual("Debug", entry.Category);
            Assert.AreEqual("Debug info for abiword", entry.ShortDescription);
            Assert.IsTrue(entry.Requires.Length == 1);
            Assert.AreEqual("cygwin-debuginfo", entry.Requires[0]);
            Assert.AreEqual("3.0.2-2", entry.LatestVersion);
            Assert.IsTrue(entry.InstallVersions.Count == 3);
        }

        [Test]
        public void TestWebPackageList()
        {
            var reader = new CygwinIniReader();
            var packages = reader.Read(new Uri("https://cygwin.com/pub/cygwin/x86_64/setup.ini"));
            Assert.IsNotNull(packages);
        }

        [Test]
        public void TestFilePackageList()
        {
            var inifile = GetTestDataPath("HintFiles", "setup.ini");
            var reader = new CygwinIniReader();

            var packages = reader.Read(inifile);
            Assert.IsNotNull(packages);
            Assert.IsTrue(packages.Count == 10274);
            foreach (var package in packages.Values)
            {
                Assert.IsNotNull(package.Name);
                Assert.IsNotNull(package.Category);
                Assert.IsNotNull(package.Requires);
                Assert.IsNotNull(package.ShortDescription);
                Assert.IsNotNull(package.InstallVersions);
                Assert.IsNotNull(package.LatestVersion);
                Assert.IsNotNull(package.InstallVersions[package.LatestVersion]);
            }
        }
    }
}
